﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TicTacToe
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public enum typ
    {
        pusty,
        OOOOO,
        XXXXX,
    }
    public partial class MainWindow : Window
    {
        private typ[] tRezultaty;  // Przetrzymuje ustawienia znaków na polach planszy 0-8
        private bool turaGracza1 = true;  // Jeśli TRUE to tura Gracza1 "O" // Jeśli FALSE to tura Gracza2 "X"
        private bool czyKoniec;  // True jeśli zakończona
        private bool czyWygral1 = false;  // Jeśli TRUE to wygrał Gracza1 "O" // Jeśli FALSE to wygrał Gracza2 "X"
        private bool remis = false;
        private int kolko = 0; // liczniki
        private int krzyzyk = 0; // liczniki

        public MainWindow()
        {
            InitializeComponent();
            NowaGra();

        }
        private void NowaGra()
        {
            Dodaj();  //ustawiamy zera lub dodajemy punkty z poprzedniej tury
            czyKoniec = false;  // resetujemy zmienną czyKoniec
            tRezultaty = new typ[9]; //Tworzymy nową tablicę pustych pól
            for (var i = 0; i < tRezultaty.Length; i++)  //Upewniamy się że są puste, ustawiając je jeszcze raz na "puste"
            {
                tRezultaty[i] = typ.pusty;  
            }
            turaGracza1 = czyWygral1 ? false : true;  //Upewniamy się że Gracz1 rozpoczyna grę pierwszy
            Wyczysc();  //upewniamy się że nie ma żadnych znaków na planszy i że kolor planszy został zresetowany
        }
        private void PClick(object sender, RoutedEventArgs e)
        {
            //Sprawdzamy czy gra się już nie zakończyła
            //TRUE rozpoczyna nową grę
            if (czyKoniec)
            {
                NowaGra();
                return;
            }
            var pole = (Button)sender;  //Przypisujemy zmiennej pole przycisk który wysłał kliknięcie

            //Szukamy pozycje przycisku w tablicy 0-8
            var kolumna = Grid.GetColumn(pole);
            var wiersz = Grid.GetRow(pole);
            var indeks = kolumna + (wiersz * 3);

            //Jeśli pole jest już zajęte nie rób nic
            if (tRezultaty[indeks] != typ.pusty)
                return;

            //Ustawiamy wartosc pola na podstawie tego który gracz ma ruch
            //Jest to skrócony if sprawdzamy czy turaGracza1 jest true:false
            tRezultaty[indeks] = turaGracza1 ? typ.OOOOO : typ.XXXXX;
            //Na tej podstawie ustawiamy znak
            pole.Content = turaGracza1 ? "O" : "X";

            //Zmieniamy kolor X'ów na czerwone i znaczników czyja tura
            if (!turaGracza1)
            {
                pole.Foreground = Brushes.DarkRed;
                Tura.Content = "O";
                Tura.Foreground = Brushes.DarkGreen;

            }
            else
            {
                Tura.Content = "X";
                Tura.Foreground = Brushes.DarkRed;
            }
            //Sprawdzamy czy jest zwycięzca 
            Sprawdz();
            //Zmieniamy tury
            turaGracza1 = turaGracza1 ? false : true;
        }// Co sie dzieje po kliknięciu na pole
        private void Sprawdz()
        {
            
            #region Poziomo
            //
            // Wiersz 0 ---------------------------------------------------------------------------------------------------------------------
            //
            if (tRezultaty[0] != typ.pusty && (tRezultaty[0] & tRezultaty[1] & tRezultaty[2]) == tRezultaty[0])
            {
                //Koniec gry
                czyKoniec = true;
                czyWygral1 = turaGracza1 ? true : false;
                //zmieniamy background wygranego rzędu
                p0.Background = p1.Background = p2.Background = Brushes.Gold;
                return;
            }
            //
            // Wiersz 2 ---------------------------------------------------------------------------------------------------------------------
            //
            else if (tRezultaty[3] != typ.pusty && (tRezultaty[3] & tRezultaty[4] & tRezultaty[5]) == tRezultaty[3])
            {
                //Koniec gry
                czyKoniec = true;
                czyWygral1 = turaGracza1 ? true : false;
                //zmieniamy background wygranego rzędu
                p3.Background = p4.Background = p5.Background = Brushes.Gold;
                return;
            }
            //
            // Wiersz 3 ---------------------------------------------------------------------------------------------------------------------
            //
            else if (tRezultaty[6] != typ.pusty && (tRezultaty[6] & tRezultaty[7] & tRezultaty[8]) == tRezultaty[6])
            {
                //Koniec gry
                czyKoniec = true;
                czyWygral1 = turaGracza1 ? true : false;
                //zmieniamy background wygranego rzędu
                p6.Background = p7.Background = p8.Background = Brushes.Gold;
                return;
            }
            #endregion

            #region Pionowo
            //
            // Kolumna 0 -------------------------------------------------------------------------------------------------------------------
            //
            else if (tRezultaty[0] != typ.pusty && (tRezultaty[0] & tRezultaty[3] & tRezultaty[6]) == tRezultaty[0])
            {
                //Koniec gry
                czyKoniec = true;
                czyWygral1 = turaGracza1 ? true : false;
                //zmieniamy background wygranego rzędu
                p0.Background = p3.Background = p6.Background = Brushes.Gold;
                return;
            }
            //
            // Kolumna 1 -------------------------------------------------------------------------------------------------------------------
            //
            else if (tRezultaty[1] != typ.pusty && (tRezultaty[1] & tRezultaty[4] & tRezultaty[7]) == tRezultaty[1])
            {
                //Koniec gry
                czyKoniec = true;
                czyWygral1 = turaGracza1 ? true : false;
                //zmieniamy background wygranego rzędu
                p1.Background = p4.Background = p7.Background = Brushes.Gold;
                return;
            }
            //
            // Kolumna 2 -------------------------------------------------------------------------------------------------------------------
            //
            else if (tRezultaty[2] != typ.pusty && (tRezultaty[2] & tRezultaty[5] & tRezultaty[8]) == tRezultaty[2])
            {
                //Koniec gry
                czyKoniec = true;
                czyWygral1 = turaGracza1 ? true : false;
                //zmieniamy background wygranego rzędu
                p2.Background = p5.Background = p8.Background = Brushes.Gold;
                return;
            }
            #endregion

            #region Na ukos
            //
            // Lewa Góra - prawy dół -------------------------------------------------------------------------------------------------------
            //
            else if (tRezultaty[0] != typ.pusty && (tRezultaty[0] & tRezultaty[4] & tRezultaty[8]) == tRezultaty[0])
            {
                //Koniec gry
                czyKoniec = true;
                czyWygral1 = turaGracza1 ? true : false;
                //zmieniamy background wygranego rzędu
                p0.Background = p4.Background = p8.Background = Brushes.Gold;
                return;
            }
            //
            // Prawa Góra - lewy dół -------------------------------------------------------------------------------------------------------
            //
            else if (tRezultaty[2] != typ.pusty && (tRezultaty[2] & tRezultaty[4] & tRezultaty[6]) == tRezultaty[2])
            {
                //Koniec gry
                czyKoniec = true;
                czyWygral1 = turaGracza1 ? true : false;
                //zmieniamy background wygranego rzędu
                p2.Background = p4.Background = p6.Background = Brushes.Gold;
                return;
            }
            #endregion

            #region Remis
            if (!tRezultaty.Any(result => result == typ.pusty))
            {
                //Koniec gry
                czyKoniec = true;
                remis = true;
                czyWygral1 = turaGracza1 ? true : false;
                //zmieniamy background na pomarańczowy
                Remisuj();
            }
            #endregion

        }// Sprawdza czy Ktoś wygrał układając 3 symbole w prostej linii
        private void Dodaj()
        {
            if (!remis && czyKoniec)
            {
                if (czyWygral1) kolko++;
                else krzyzyk++;
            }
            else remis = false;
            punktO.Content = kolko.ToString();
            punktX.Content = krzyzyk.ToString();
        }
        private void Wyczysc()
        {
            p0.Content = string.Empty;
            p0.Background = Brushes.White;
            p0.Foreground = Brushes.Green;

            p1.Content = string.Empty;
            p1.Background = Brushes.White;
            p1.Foreground = Brushes.Green;


            p2.Content = string.Empty;
            p2.Background = Brushes.White;
            p2.Foreground = Brushes.Green;


            p3.Content = string.Empty;
            p3.Background = Brushes.White;
            p3.Foreground = Brushes.Green;


            p4.Content = string.Empty;
            p4.Background = Brushes.White;
            p4.Foreground = Brushes.Green;

            p5.Content = string.Empty;
            p5.Background = Brushes.White;
            p5.Foreground = Brushes.Green;


            p6.Content = string.Empty;
            p6.Background = Brushes.White;
            p6.Foreground = Brushes.Green;


            p7.Content = string.Empty;
            p7.Background = Brushes.White;
            p7.Foreground = Brushes.Green;


            p8.Content = string.Empty;
            p8.Background = Brushes.White;
            p8.Foreground = Brushes.Green;

        }
        private void Remisuj()
        {
            p0.Background = Brushes.Orange;
            p0.Foreground = Brushes.Black;

            p1.Background = Brushes.Orange;
            p1.Foreground = Brushes.Black;

            p2.Background = Brushes.Orange;
            p2.Foreground = Brushes.Black;

            p3.Background = Brushes.Orange;
            p3.Foreground = Brushes.Black;

            p4.Background = Brushes.Orange;
            p4.Foreground = Brushes.Black;

            p5.Background = Brushes.Orange;
            p5.Foreground = Brushes.Black;

            p6.Background = Brushes.Orange;
            p6.Foreground = Brushes.Black;

            p7.Background = Brushes.Orange;
            p7.Foreground = Brushes.Black;

            p8.Background = Brushes.Orange;
            p8.Foreground = Brushes.Black;
        }
        private void ResetPunkt(object sender, RoutedEventArgs e)
        {
            kolko = 0;
            krzyzyk = 0;
            czyWygral1 = true;
            NowaGra();
        }
        private void Nowa(object sender, RoutedEventArgs e)
        {
            NowaGra();
        }
    }
}
